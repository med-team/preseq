Source: preseq
Section: science
Priority: optional
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>
Build-Depends: debhelper-compat (= 13),
               libhts-dev
Standards-Version: 4.5.0
Homepage: http://smithlabresearch.org/software/preseq/
Vcs-Browser: https://salsa.debian.org/med-team/preseq
Vcs-Git: https://salsa.debian.org/med-team/preseq.git
Rules-Requires-Root: no

Package: preseq
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: determine redundancy in RNAseq experiments
 The preseq package is aimed at predicting and estimating the complexity
 of a genomic sequencing library, equivalent to predicting and estimating
 the number of redundant reads from a given sequencing depth and how
 many will be expected from additional sequencing using an initial
 sequencing experiment. The estimates can then be used to examine the
 utility of further sequencing, optimize the sequencing depth, or to
 screen multiple libraries to avoid low complexity samples.
 .
 Preseq package also predicts the genomic coverage for single-cell
 DNA sequencing experiments with the more recently added gc_extrap
 functionality.
